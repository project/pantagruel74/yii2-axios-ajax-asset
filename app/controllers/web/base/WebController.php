<?php

namespace app\controllers\web\base;

use yii;

/**
 * Прототип контроллера с контроолем доступа по правам
 */
abstract class WebController extends \yii\web\Controller
{

    /**
     * @return array
     */
    abstract public function permissions(): array;

    /**
     * @param $action
     * @return bool
     * @throws yii\web\BadRequestHttpException
     * @throws \Exception
     */
    public function beforeAction($action): bool
    {
        $actionId = $action->id;
        if(isset($this->permissions()[$actionId])) {
            $user = \Yii::$app->user;
            foreach ($this->permissions()[$actionId] as $permission)
            {
                if($user->can($permission)) {
                    return parent::beforeAction($action);
                }
                if(($permission === '*')) {
                    return parent::beforeAction($action);
                }
                if(($permission === '?') && ($user->isGuest === true)) {
                    return parent::beforeAction($action);
                }
                if(($permission === '@') && ($user->isGuest !== true)) {
                    return parent::beforeAction($action);
                }
            }
        }
        $this->goHome();
        return false;
    }

    /**
     * @return yii\web\Response
     */
    public function goHome(): yii\web\Response
    {
        $user = \Yii::$app->user;
        $startPages = \Yii::$app->params['startPagesForRoles'];
        if($user->isGuest === true) {
            return $this->redirect($startPages['?']);
        }
        return $this->redirect($startPages['@']);
    }

}