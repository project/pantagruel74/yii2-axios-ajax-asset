<?php

namespace app\controllers\web\base;

abstract class ApiController extends WebController
{

    /**
     * @param string $msg
     * @return bool
     */
    protected function denyAccess(string $msg): bool
    {
        print_r(json_encode((object) [
            "error" => 'access denied'
        ])); die();
    }

    /**
     * @param $route
     * @param $params
     * @return mixed|void|null
     */
    public function run($route, $params = [])
    {
        try {
            return parent::run($route, $params);
        } catch (\Exception $exception) {
            print_r("{'error': '" . $exception->getMessage() . "'}");die();
        }
    }
}