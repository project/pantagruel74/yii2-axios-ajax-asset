<?php

namespace app\controllers\web;

use app\controllers\web\base\WebController;
use Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\TestPresenter;
use yii\web\Response;

class SiteController extends WebController
{

    /**
     * @return \string[][]
     */
    public function permissions(): array
    {
        return [
            'index' => ['*'],
            'save-get' => ['*'],
            'save-post' => ['*'],
            'save-file' => ['*'],
            'btn-get' => ['*'],
            'btn-post' => ['*'],
            'save-file-ajax' => ['*'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions(): array
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     * @throws \Throwable
     */
    public function actionIndex(): string
    {
        return $this->renderContent(
            \Yii::$app->testPresenter->renderIndexScreen($this->getView())
        );
    }

    public function actionSaveGet(): Response
    {
        $answer = \Yii::$app->testPresenter->applyGetForm($this->request);
        return $this->asJson($answer);
    }

    public function actionSavePost(): Response
    {
        $answer = \Yii::$app->testPresenter->applyPostForm($this->request);
        return $this->asJson($answer);
    }

    public function actionSaveFile(): Response
    {
        $answer = \Yii::$app->testPresenter->applyFileForm($this->request);
        return $this->asJson($answer);
    }

    public function actionSaveFileAjax(): Response
    {
        $answer = \Yii::$app->testPresenter->applyFileFormAjax($this->request);
        return $this->asJson($answer);
    }

    public function actionBtnGet(): Response
    {
        $answer = \Yii::$app->testPresenter->applyBtnGet($this->request);
        return $this->asJson($answer);
    }

    public function actionBtnPost(): Response
    {
        $answer = \Yii::$app->testPresenter->applyBtnPost($this->request);
        return $this->asJson($answer);
    }

}
