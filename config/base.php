<?php
$ds = DIRECTORY_SEPARATOR;
$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__) . $ds . 'app',
    'runtimePath' => dirname(__DIR__) . $ds . 'runtime',
    'vendorPath' => dirname(__DIR__) . $ds . 'vendor',
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => [],
    ],
];

return $config;