<?php
return [
    'registeredHomepage' => '/site/index',
    'guestHomepage' => '/site/index',
    'startPagesForRoles' => [
        '?' => '/site/index',
        '@' => '/site/index'
    ],
];