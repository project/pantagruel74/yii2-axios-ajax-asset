<?php

class Yii {
    /**
     * @var \yii\web\Application|\yii\console\Application|__Application
     */
    public static $app;
}

/**
 * @property yii\rbac\DbManager $authManager 
 * @property \yii\web\User|__WebUser $user
 * @property \Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\TestPresenter $testPresenter
 */
class __Application {
}

/**
 * @property app\models\User $identity
 */
class __WebUser {
}
