function sendAjax(uri, params, method)
{
    if(!method) method = 'get';
    if((params instanceof FormData) && (method === 'get')) {
        const formDataObj = {};
        params.forEach((v, k) => (formDataObj[k] = v));
        params = formDataObj;
    }
    let reqParams = {
        method: method,
        type: method,
        url: uri,
        params: method !== 'get' ? null : params,
        data: method !== 'get' ? params : null,
    };
    if(method !== 'get') {
        reqParams['headers'] = {"Content-Type": "multipart/form-data"};
    }
    return axios(reqParams);
}

function sendDataAjax(data, formParams, reactionsObj)
{
    sendAjax(formParams.action ?? '', data, formParams.method ?? 'get')
        .then(function(response) {
            for(let key in reactionsObj)
            {
                if(response.data[key]) {
                    reactionsObj[key](response.data);
                }
            }
        })
}

function sendContainerDataAjax(container, formParams = {}, extraParams = {}, reactionsObj = {}) {
    const formData = new FormData();
    for(let p in extraParams) {
        formData.append(p, extraParams[p]);
    }
    Array.from(container.getElementsByTagName('input')).forEach((input) => {
        if(input.type === 'checkbox') {
            formData.append(input.name, input.checked ? input.value : 0);
        } else if (input.type === 'radio') {
            if(input.checked) {
                formData.append(input.name, input.value);
            }
        } else if(input.type === 'file') {
            if(input.files.length === 1) {
                formData.append(input.name, input.files[0]);
            } else {
                // formData.append(input.name + '[]', input.files);
                Array.from(input.files).forEach((el) => formData.append(input.name + '[]', el));
            }
        } else {
            formData.append(input.name, input.value);
        }
    });
    Array.from(container.getElementsByTagName('textarea')).forEach((textarea) => {
        formData.append(textarea.name, textarea.value);
    });
    Array.from(container.getElementsByTagName('select')).forEach((select) => {
        formData.append(select.name, select.value);
    });
    sendDataAjax(formData, formParams, reactionsObj);
}

function sendFormAjax(form, extraParams = {}, reactionsObj = {}) {
    sendContainerDataAjax(
        form,
        {action: form.action ?? '', method: form.method ?? 'get'},
        extraParams,
        reactionsObj
    )
}