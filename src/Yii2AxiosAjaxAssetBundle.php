<?php

namespace Pantagruel74\AxiosAjaxAsset;

use yii\web\AssetBundle;

class Yii2AxiosAjaxAssetBundle extends AssetBundle
{
    public $sourcePath = __DIR__ . DIRECTORY_SEPARATOR . 'bundle';

    public $js = [
        'js/yii2-axios-ajax-asset.js',
        'https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js',
    ];
}