# Yii2-axios-ajax-asset

## Requirements

1. PHP >=7.4
2. Composer >=2.0
3. Yii2 project
4. Apache or NGINX server


## Files structure

Source code, assets and helpers contains into "src" folder.

All other folders in package need for Acceptance testing of package.


## Usage

1. Connect asset bundle by one of this way
   1. Register asset-bundle into your view like this: `Yii2AxiosAjaxAssetBundle.register($this);`
   2. Register asset-bundle into your controller like this: `Yii2AxiosAjaxAssetBundle.register($this->getView());`
   3. Add asset bundle as dependency to your main asset bundle

2. Use js functions which this bundle allows you:
   1. `sendAjax(url, data, method)` - sending ajax with data or params to Url, and return axios promise.
      1. param `url` is target url
      2. param `data` is object of params like `{p1: v1, p2: v2}`, or FormData object
      3. param `method` is string shows request method like 'get' or 'post'
      4. return AxiosPromise
      5. example: `sendAjax('/site/get-form-data', {id: 12}, 'get').then((res) => {if(res.data.html) console.log(res.data.html)});`
   2. `sendDataAjax(data, formParams, reactionsObj)` - sending ajax with registered response data reaction. Returns void.
      1. param `data` is object of params like `{p1: v1, p2: v2}`, or FormData object
      2. param `formParams` is object of form settings like `{action: '/some/action', method: 'get'}`
      3. param `reactionsObj` is object registering reaction of response, like `{html: (res) => console.log(res.html), error: (res) => {window.alert("Error: " + res.error)}}`
      4. return void
      5. example: `sendDataAjax({id: 21}, {action: '/site/get-form-data', method: 'get'}, {html: (resData) => console.log(resData.html)})`
   3. `sendContainerDataAjax(container, formParams = {}, extraParams = {}, reactionsObj = {})` - collects form-elements-data in selected dom-element and send it ajax with registered reaction
      1. param `container` - dom-element contains form-elements to collect and send
      2. param `formParams` is object of form settings like `{action: '/some/action', method: 'get'}`
      3. param `extraParams` is object of extra params to add to collected from form-elements data
      4. param `reactionsObj` is object registering reaction of response, like `{html: (res) => console.log(res.html), error: (res) => {window.alert("Error: " + res.error)}}`
      5. return void
      6. example: `<div><input type="text" onchange="sendContainerDataAjax(this.closest('div'), {action: '/site/get-form-data', method: 'get'}, {id: 12}, {html: (resData) => console.log(resData.html)})"></div>`
   4. `sendFormAjax(form, extraParams = {}, reactionsObj = {})` - collect form-elements-data from selected form and send it ajax with registered reaction
      1. param `form` - form dom element
      2. param `extraParams` is object of extra params to add to collected from form-elements data
      3. param `reactionsObj` is object registering reaction of response, like `{html: (res) => console.log(res.html), error: (res) => {window.alert("Error: " + res.error)}}`
      4. return void
      5. example: `<form action="/site/get-form-data" method="get"><input type="text" onchange="sendContainerDataAjax(this.closest('form'), {id: 12}, {html: (resData) => console.log(resData.html)})"></form>`

## License

- MIT


## Author

- Anatoly Starodubtsev <Pantagruel74>
- Tostar74@mail.ru