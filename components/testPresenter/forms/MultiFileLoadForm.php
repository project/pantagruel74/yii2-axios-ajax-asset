<?php

namespace Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\forms;

use Pantagruel74\Yii2Strictly\StrictlyModelTrait;
use Pantagruel74\Yii2Strictly\UploadFileStrictlyTrait;
use yii\base\Model;
use yii\web\UploadedFile;

class MultiFileLoadForm extends Model
{
    use UploadFileStrictlyTrait;
    use StrictlyModelTrait;

    public $file = null;

    public function rules(): array
    {
        return [
            ['file', 'file', 'skipOnEmpty' => false],
            ['file', 'safe'],
        ];
    }
}