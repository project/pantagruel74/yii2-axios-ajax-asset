<?php

namespace Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\forms;

use Pantagruel74\Yii2Strictly\StrictlyModelTrait;
use yii\base\Model;
use yii\web\UploadedFile;

class TestForm extends Model
{
    use StrictlyModelTrait;

    public string $text = '';
    public string $bigText = '';
    public int $num = 0;

    public function rules(): array
    {
        return [
            [['text', 'num', 'bigText'], 'required'],
        ];
    }
}