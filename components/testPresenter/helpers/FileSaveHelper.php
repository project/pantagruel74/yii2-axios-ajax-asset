<?php

namespace Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\helpers;

use Ramsey\Uuid\Uuid;
use Webmozart\Assert\Assert;
use yii\web\UploadedFile;

class FileSaveHelper
{
    public static function saveFileStrictly(UploadedFile $file): string
    {
        $ds = DIRECTORY_SEPARATOR;
        $filesDir = \Yii::$app->getRuntimePath() . $ds . 'files';
        if(!is_dir($filesDir)) mkdir($filesDir);
        $filePath = $filesDir . $ds . Uuid::uuid4()->toString() . '.' . $file->getExtension();
        $saveResult = $file->saveAs($filePath);
        Assert::true($saveResult, "File saving problem. Code: " . $file->error);
        return $filePath;
    }
}