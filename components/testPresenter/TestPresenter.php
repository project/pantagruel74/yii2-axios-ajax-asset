<?php

namespace Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter;

use Pantagruel74\AxiosAjaxAsset\Yii2AxiosAjaxAssetBundle;
use Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\forms\FileLoadForm;
use Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\forms\MultiFileLoadForm;
use Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\forms\TestForm;
use Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\helpers\FileSaveHelper;
use Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\widgets\ButtonsPanelWidget;
use Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\widgets\FileLoadFormWidget;
use Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\widgets\FileLoadOnChangeFormWidget;
use Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\widgets\IndexScreenWidget;
use Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\widgets\TestAjaxButtonWidget;
use Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\widgets\TestDataAjaxButtonWidget;
use Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\widgets\TestFormWidget;
use yii\base\Component;
use yii\web\Request;
use yii\web\UploadedFile;
use yii\web\View;

class TestPresenter extends Component
{

    const TEST_PARAM = 'param';
    const TEST_PARAM_VAL = 'aboba';


    public function renderIndexScreen(View $view): string
    {
        Yii2AxiosAjaxAssetBundle::register($view);
        $getFormHtml = TestFormWidget::widget([
            'formModel' => new TestForm(),
            'actionUrl' => '/site/save-get',
            'method' => 'get',
            'param' => self::TEST_PARAM,
            'paramVal' => self::TEST_PARAM_VAL,
            'answerParam' => 'ans'
        ]);
        $postFormHtml = TestFormWidget::widget([
            'formModel' => new TestForm(),
            'actionUrl' => '/site/save-post',
            'method' => 'post',
            'param' => self::TEST_PARAM,
            'paramVal' => self::TEST_PARAM_VAL,
            'answerParam' => 'ans'
        ]);
        $sendAjaxDataGetBtnHtml = TestDataAjaxButtonWidget::widget([
            'actionUrl' => '/site/btn-get',
            'method' => 'get',
            'param' => self::TEST_PARAM,
            'paramVal' => self::TEST_PARAM_VAL,
            'content' => 'sendAjaxData:get',
        ]);
        $sendAjaxDataPostBtnHtml = TestDataAjaxButtonWidget::widget([
            'actionUrl' => '/site/btn-post',
            'method' => 'post',
            'param' => self::TEST_PARAM,
            'paramVal' => self::TEST_PARAM_VAL,
            'content' => 'sendAjaxData:post',
        ]);
        $btnsPanel = ButtonsPanelWidget::widget([
            'buttonHtmls' => [
                $sendAjaxDataGetBtnHtml,
                $sendAjaxDataPostBtnHtml,
            ],
        ]);
        $fileLoadModel = new FileLoadForm();
        $fileLoadFormHtml = FileLoadFormWidget::widget([
            'action' => '/site/save-file',
            'model' => $fileLoadModel,
            'answerParam' => 'ans',
        ]);
        $fileLoadOnChangeForm = FileLoadOnChangeFormWidget::widget([
            'action' => '/site/save-file-ajax',
            'model' => new MultiFileLoadForm(),
            'answerParam' => 'ans',
        ]);
        return IndexScreenWidget::widget([
            'panels' => [
                $getFormHtml,
                $postFormHtml,
                $btnsPanel,
                $fileLoadFormHtml,
                $fileLoadOnChangeForm,
            ]
        ]);
    }


    public function applyGetForm(Request $request): array
    {
        $getFormModel = new TestForm();
        $data = $request->get();
        $getFormModel->loadStrictly($data);
        $directlyGetParam = $data[self::TEST_PARAM] ?? null;
        return [
            'ans' => array_merge(
                $getFormModel->getAttributes(),
                [self::TEST_PARAM => $directlyGetParam]
            )
        ];
    }


    public function applyPostForm(Request $request): array
    {
        $postFormModel = new TestForm();
        $data = $request->post();
        $postFormModel->loadStrictly($data);
        $directlyGetParam = $data[self::TEST_PARAM] ?? null;
        return [
            'ans' => array_merge(
                $postFormModel->getAttributes(),
                [self::TEST_PARAM => $directlyGetParam]
            )
        ];
    }


    public function applyFileForm(Request $request): array
    {
        $fileForm = new FileLoadForm();
        $fileForm->uploadFileStrictly('file');
        $filePath = FileSaveHelper::saveFileStrictly($fileForm->file);
        return [
            'ans' => $filePath,
        ];
    }

    public function applyFileFormAjax(Request $request): array
    {
        $fileForm = new MultiFileLoadForm();
        $fileForm->file = UploadedFile::getInstances($fileForm, 'file');
        return [
            'ans' => print_r($fileForm->file, true),
        ];
    }


    public function applyBtnGet(Request $request): array
    {
        $data = $request->get();
        return [
            'ans' => $data,
        ];
    }


    public function applyBtnPost(Request $request): array
    {
        $data = $request->post();
        return [
            'ans' => $data,
        ];
    }

}