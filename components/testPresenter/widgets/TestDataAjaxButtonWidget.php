<?php

namespace Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\widgets;

use Mnemesong\Microwidget\traits\MicrowidgetTrait;
use yii\base\Widget;

class TestDataAjaxButtonWidget extends Widget
{
    use MicrowidgetTrait;

    public string $actionUrl;
    public string $method;
    public string $param;
    public string $paramVal;
    public string $content;

    public function run(): string
    {
        return $this->r(function () {
            ?>
            <button onclick="sendDataAjax(
                {
                    <?= $this->param ?>: '<?= $this->paramVal ?>',
                    <?= \Yii::$app->request->csrfParam ?>: '<?= \Yii::$app->request->csrfToken ?>'
                },
                {action: '<?= $this->actionUrl ?>', method: '<?= $this->method ?>'},
                {ans: (res) => console.log(res)}
            )"><?= $this->content ?></button>
            <?php
        });
    }
}