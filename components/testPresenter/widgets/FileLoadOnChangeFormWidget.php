<?php

namespace Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\widgets;

use Mnemesong\Microwidget\traits\MicrowidgetTrait;
use Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\forms\FileLoadForm;
use Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\forms\MultiFileLoadForm;
use yii\base\Widget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

class FileLoadOnChangeFormWidget extends Widget
{
    use MicrowidgetTrait;

    public MultiFileLoadForm $model;
    public string $action;
    public string $answerParam;

    public function run(): string
    {
        return $this->r(function () {
            $csrfParam = \Yii::$app->request->csrfParam;
            $csrfToken = \Yii::$app->request->csrfToken;
            ?>
            <div style="display: grid; grid-gap: 20px;">
                <h3 style="margin-bottom: 0">File onchange load</h3>
                <div style="display: grid; grid-gap: 10px; grid-template-columns: 100px 1fr;">
                    <?= Html::activeLabel($this->model, 'file') ?>
                    <?= Html::activeFileInput($this->model, 'file', [
                        'onchange' => "sendContainerDataAjax("
                            . "this.closest('div'), "
                            . "{action: '" . $this->action . "', method: 'post'}, "
                            . "{" . $csrfParam . ": '" . $csrfToken . "'}, "
                            . "{" . $this->answerParam . ": (res) => console.log(res." . $this->answerParam . ")}"
                            . ")",
                        "multiple" => true,
                    ]) ?>
                </div>
            </div>
            <?php
        });
    }
}