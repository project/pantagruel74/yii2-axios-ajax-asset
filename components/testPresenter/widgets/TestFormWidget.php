<?php

namespace Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\widgets;

use Mnemesong\Microwidget\traits\MicrowidgetTrait;
use Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\forms\TestForm;
use yii\base\Widget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

class TestFormWidget extends Widget
{
    use MicrowidgetTrait;

    public TestForm $formModel;
    public string $actionUrl;
    public string $method;
    public string $param;
    public string $paramVal;
    public string $answerParam;

    public function run(): string
    {
        return $this->r(function () {
            ActiveForm::begin([
                'action' => $this->actionUrl,
                'method' => $this->method,
            ]);
            ?>
            <div style="display: grid; grid-gap: 20px;">
                <h3 style="margin-bottom: 0;"><?= ucfirst($this->method) ?> form</h3>
                <div style="display: grid; grid-gap: 10px; grid-template-columns: 100px 1fr;">
                    <?= Html::activeLabel($this->formModel, 'text') ?>
                    <?= Html::activeTextInput($this->formModel, 'text') ?>
                </div>
                <div style="display: grid; grid-gap: 10px; grid-template-columns: 100px 1fr;">
                    <?= Html::activeLabel($this->formModel, 'num') ?>
                    <?= Html::activeTextInput($this->formModel, 'num') ?>
                </div>
                <div style="display: grid; grid-gap: 10px; grid-template-columns: 100px 1fr;">
                    <?= Html::activeLabel($this->formModel, 'bigText') ?>
                    <?= Html::activeTextarea($this->formModel, 'bigText') ?>
                </div>
                <button type="button" onclick="sendFormAjax(
                    this.closest('form'),
                    {<?= $this->param ?>: '<?= $this->paramVal ?>'},
                    {<?= $this->answerParam ?>: (res) => console.log(res.<?= $this->answerParam ?>)}
                )">Submit</button>
            </div>
            <?php
            ActiveForm::end();
        });
    }
}