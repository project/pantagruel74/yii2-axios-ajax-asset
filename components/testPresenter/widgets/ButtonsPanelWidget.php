<?php

namespace Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\widgets;

use Mnemesong\Microwidget\traits\MicrowidgetTrait;
use yii\base\Widget;

class ButtonsPanelWidget extends Widget
{
    use MicrowidgetTrait;

    public array $buttonHtmls = [];

    public function run(): string
    {
        return $this->r(function () {
            ?>
            <div style="display: grid; grid-gap: 20px;">
                <h3 style="margin-bottom: 0;">Buttons panel</h3>
                <?= implode('', $this->buttonHtmls) ?>
            </div>
            <?php
        });
    }
}