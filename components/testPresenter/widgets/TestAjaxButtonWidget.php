<?php

namespace Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\widgets;

use Mnemesong\Microwidget\traits\MicrowidgetTrait;
use yii\base\Widget;

class TestAjaxButtonWidget extends Widget
{
    use MicrowidgetTrait;

    public string $actionUrl;
    public string $method;
    public string $param;
    public string $paramVal;
    public string $content;

    public function run(): string
    {
        return $this->r(function () {
            ?>
            <button onclick="sendAjax(
                '<?= $this->actionUrl ?>',
                {
                    <?= $this->param ?>: '<?= $this->paramVal ?>',
                    <?= \Yii::$app->request->csrfParam ?>: '<?= \Yii::$app->request->csrfToken ?>'
                },
                '<?= $this->method ?>'
            ).then((res) => console.log(res.data))"><?= $this->content ?></button>
            <?php
        });
    }
}