<?php

namespace Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\widgets;

use Mnemesong\Microwidget\traits\MicrowidgetTrait;
use yii\base\Widget;

class IndexScreenWidget extends Widget
{
    use MicrowidgetTrait;

    public array $panels = [];

    public function run(): string
    {
        return $this->r(function () {
            ?>
                <div style="padding: 60px;">
                    <div style="display: grid; grid-gap: 60px; grid-template-columns: repeat(auto-fill, 350px);">
                        <?= implode('', $this->panels) ?>
                    </div>
                </div>
            <?php
        });
    }
}