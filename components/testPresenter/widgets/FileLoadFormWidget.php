<?php

namespace Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\widgets;

use Mnemesong\Microwidget\traits\MicrowidgetTrait;
use Pantagruel74\AxiosAjaxAssetTestComponents\testPresenter\forms\FileLoadForm;
use yii\base\Widget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

class FileLoadFormWidget extends Widget
{
    use MicrowidgetTrait;

    public FileLoadForm $model;
    public string $action;
    public string $answerParam;

    public function run(): string
    {
        return $this->r(function () {
            ActiveForm::begin([
                'method' => 'post',
                'action' => $this->action,
            ])
            ?>
            <div style="display: grid; grid-gap: 20px;">
                <h3 style="margin-bottom: 0">File form</h3>
                <div style="display: grid; grid-gap: 10px; grid-template-columns: 100px 1fr;">
                    <?= Html::activeLabel($this->model, 'file') ?>
                    <?= Html::activeFileInput($this->model, 'file') ?>
                </div>
                <button type="button" onclick="sendFormAjax(
                    this.closest('form'),
                    {},
                    {<?= $this->answerParam ?>: (res) => console.log(res.<?= $this->answerParam ?>)}
                )">Submit</button>
            </div>
            <?php
            ActiveForm::end();
        });
    }
}